// ამოცანა #1
let names = [
  "George",
  "Nick",
  "Tom",
  "Kate",
  "Annie",
  "James",
  "Will",
  "Jack",
  "Nate",
  "Edward",
];

// const friends = names.filter(el => {
//     return el.length === 4;
// })
// console.log(friends);

const checkFriends = (names) => {
  let friends = [];
  for (let i = 0; i < names.length; i++) {
    if (names[i].length === 4) {
      friends.push(names[i]);
    }
  }
  return friends;
};
console.log(checkFriends(names));

// ამოცანა #2
let numArrayOne = [5, 8, 12, 19, 22];
let numArrayTwo = [52, 76, 14, 12, 4];
let numArrayThree = [3, 87, 45, 12, 7];
function sumOfMinTwo(arr) {
  arr.sort((a, b) => {
    return a - b;
  });
  return arr[0] + arr[1];
}
console.log(sumOfMinTwo(numArrayOne));
console.log(sumOfMinTwo(numArrayTwo));
console.log(sumOfMinTwo(numArrayThree));
